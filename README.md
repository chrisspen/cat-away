Cat-Away
========

Description
-----------

These are the plans for a small device that attaches to a cabinet
overtop a kitchen counter and keeps a cat off the counter.

It contains two IR distance rangerfinders. Sensor A, pointing straight out.
Another, sensor B, pointing down at an angle. Both sensors feed into a small Arduino microcomputer.

When a cat jumps on the counter, it triggers sensor B but not sensor A.

The Arduino then activates a piezo buzzer to annoy the cat, making it jump back down.

When a human steps in front of the device, it triggers sensor A.
If sensor B is then triggered for any reason, the Arduino knows not to activate the buzzer.

Parts
-----

See [bill of materials](bom.ods) for the parts you'll need.

Assembly
--------

1. Using a 3d printer, print out the chassis parts.
1. Create the wiring harness.
1. Plug the Arduino into a computer and program it using the [sketch[(sketches/CatAway/CatAway.ino).
1. Connect the Arduino, wiring harness, buzzer and IR sensors.
1. Mount the device underneath a cabinet.
1. Plug in power.
1. Test. Stand to the side, place you hand in front of the lower sensor and confirm buzzer activates. Stand in front of both sensors and confirm buzzer does not activate.
