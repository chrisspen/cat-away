/*
  Buzzer = D11
  Human IR = A2
  Cat IR = A0
*/

// Values in volts.
#define HUMAN_DETECTION_THRESHOLD 0.7
#define CAT_DETECTION_THRESHOLD 1

#define CAT_DETECTION_COUNT_MIN 3

unsigned long t0 = 0;

unsigned long last_cat_detection_count = 0;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  pinMode(11, OUTPUT);
  digitalWrite(11, LOW);

  t0 = millis();
}

void buzzer_on(){
  digitalWrite(11, HIGH);
}

void buzzer_off(){  
  digitalWrite(11, LOW);
}

// the loop function runs over and over again forever
void loop() {

  if(millis() - t0 >= 1000){
    t0 = millis();
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }

  float catValue = analogRead(A0) * (5.0 / 1023.0);
  float humanValue = analogRead(A2) * (5.0 / 1023.0);

  if(humanValue >= HUMAN_DETECTION_THRESHOLD){
    last_cat_detection_count = 0;
    buzzer_off();
  }else if(catValue >= CAT_DETECTION_THRESHOLD){
    last_cat_detection_count += 1;
    if(last_cat_detection_count >= CAT_DETECTION_COUNT_MIN){
      buzzer_on();
    }
  }else{
    last_cat_detection_count = 0;
    buzzer_off();
  }

  delay(100);
}
