SMAKN Beetle USB ATMEGA32U4 Mini Development Board for Arduino Leonardo

https://www.amazon.com/gp/product/B011AYAOHS/

*Development board size: 21mm * 28m.
*Microcontroller : ATmega32u4.
*Clock Speed : 16 MHz.
*Operating Voltage: 5V DC.
*Digital I / O Pins : 10.
*PWM Channels : 4.
*Analog Input Channels : 5.
*UART : 1.
*I2C : 1.
*Micro USB : 1.
*Flash Memory 32 KB of which 4KB used by bootloader.
*SRAM : 2.5 KB.
*EEPROM : 1 KB.

Note, before programming on Linux, make sure you remove the modemmanager package, because that will block the USB interface.
