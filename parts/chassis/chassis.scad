/*
beete: 21.25 x 18.5 x 2.5
ir sensor board: 10.6 x 33.15 x 1.62
ir sensor box: 22.05 x 8 x 7.5 => offset 7.9
buzzer d = 11.92
*/

module beetle_board(extra_h=0, extra_buff=0){
    cube([21.25+extra_buff, 18.5+extra_buff, 2.5+extra_h], center=true);
}

module ir_board(){
    cube([33.15, 10.6, 1.62], center=true);
}

module ir_holder(buzzer_clip=1, buzzer_d=11.92, show_teeth=1, show_sensor=1, mount_hole_extra=0){
    difference(){
        union(){
            if(show_sensor)
            difference(){
                cube([33.15, 10.6, 7], center=true);

                color("red")
                translate([-5.3+7.9,0,0])
                cube([22.05+.5, 8+.5, 7*2], center=true);

                color("red")
                translate([19-1.75,0,5.5-3])
                cube([5, 15, 4], center=true);
            
                // screw hole
                color("red")
                translate([-15+2.2,0,0])
                rotate([0,0,0])
                cylinder(d=2.7, h=20, center=true, $fn=50);
            }
            
            if(show_teeth)
            difference(){
                color("orange")
                translate([-18.55,0,0]){
                    translate([0,0,10])
                    rotate([0,90,0])
                    cylinder(d=10, h=4, center=true, $fn=50);
                    translate([0,0,3.25])
                    cube([4, 10, 13.5], center=true);
                    
                    translate([-2,0,10])
                    rotate([0,180,0])
                    teeth_ring(hole_d=7);
                }
                
                // axle hole
                color("red")
                translate([0,0,10])
                rotate([0,90,0])
                cylinder(d=2.7, h=200, center=true, $fn=50);
            }
        }
        
        // mount holes
        color("red")
        for(i=[-1:2:1])
        translate([-15,2.75*i,0])
        rotate([0,90,0])
        cylinder(d=2+mount_hole_extra, h=20, center=true, $fn=50);

    }
    
    // buzzer clamp
    if(buzzer_clip)
    if(show_sensor)
    translate([0,-5,0]){
        difference(){
            // buzzer clamp body
            union(){
                translate([0,-13/2/2,0])
                cube([13,13/2,7], center=true);
                translate([0,-(buzzer_d+3)/2,0])
                cylinder(d=buzzer_d+3, h=7, $fn=50, center=true);
            }
            
            // buzzer cutout
            color("red")
            translate([0,-(buzzer_d+3)/2,0])
            cylinder(d=buzzer_d+.2, h=7*2, $fn=50, center=true);

            // slot cutout
            translate([0,-13/2/2-10,0])
            cube([5,5,10], center=true);
        }
        
    }
}

module make_retainer(height=5, length=20, thickness=1, width=10){
    // top
    translate([0,-length/2+thickness/2,height])
    cube([width, length, thickness], center=true);
    
    // riser
    translate([0,0,height/2])
    cube([width, thickness, height], center=true);
    
    // bottom roll
    rotate([0,90,0])
    cylinder(d=thickness, h=width, $fn=50, center=true);
    
    // tip roll
    translate([0,-length+thickness/2,height])
    rotate([0,90,0])
    cylinder(d=thickness, h=width, $fn=50, center=true);
}

module teeth_ring(d=10, tooth_width=2, hole_d=5){
    difference(){
        intersection(){
            for(i=[0:20:360])
            rotate([i,0,0])
            rotate([0,0,45])
            cube([tooth_width, tooth_width, 100], center=true);
            
            rotate([0,90,0])
            cylinder(d=d, h=50, $fn=50);
        }
        rotate([0,90,0])
        cylinder(d=hole_d, h=50, $fn=50, center=true);
    }
}

module make_chassis(shelf_thickness=16.2, door_gap=2.5, width=10, depth=30, height=52.5){

    // under shelf
    translate([0,-depth/2,-2/2])
    cube([width, depth, 2], center=true);
    
    // front shelf
    if(1)
    translate([0,door_gap/2,-height/2+shelf_thickness])
    cube([width, door_gap, height], center=true);
    
    translate([0,door_gap/2,shelf_thickness])
    rotate([13,0,0])
    make_retainer(thickness=door_gap);
    
    for(i=[0:1:1])
    translate([-width/2/2,10/2,-10-20*i]){
        difference(){
            union(){
                cube([width/2,10,10], center=true);
                
                translate([-width/2/2,width/2,0])
                rotate([0,90,0])
                cylinder(d=width, h=width/2, $fn=50);
            }
            // axle hole
            color("red")
            translate([-10,width/2,0])
            rotate([0,90,0])
            cylinder(d=2.75+0.5, h=width*2, $fn=50);
        }
        
        translate([2.5,5,0])
        teeth_ring();
    }

    translate([6.75,-3.75,-20])
    rotate([0,90,0])
    rotate([90,0,0]){
        difference(){
            union(){
                difference(){
                    
                    color("orange")
                    beetle_board(extra_buff=5, extra_h=5);
                    
                    color("green")
                    translate([0,9.25,0])
                    scale([1,2,1])
                    beetle_board(extra_h=50);
                    
                }
                
                for(i=[-1:2:1])
                translate([9.75*i,-8.5,0])
                cube([2,2,7.5], center=true);
            }
            
            if(1)
            color("blue")
            translate([0,9.25,0])
            scale([1,2,1])
            beetle_board(extra_h=0.25, extra_buff=0.5);
        }
    }

}

color("blue")
translate([0,-100/2,16.2/2])
cube([10, 100, 16.2], center=true);

make_chassis();

translate([10,10,0]){
    
    translate([16.5,10,-10])
    rotate([90,0,0])
    ir_holder();

    if(1)
    translate([16.5,10,-30])
    rotate([90,0,0])
    ir_holder();
}

